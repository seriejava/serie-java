/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package series.java;

/**
 *
 * @author msdos
 */
public class Lista_servidores {
private String enlace;
private String idioma;

    public String getEnlace() {
        return enlace;
    }

    public void setEnlace(String enlace) {
        this.enlace = enlace;
    }

    public String getIdioma() {
        return idioma;
    }

    public void setIdioma(String idioma) {
        this.idioma = idioma;
    }

    @Override
    public String toString() {
        return "Lista_servidores{" + "enlace=" + enlace + ", idioma=" + idioma + '}';
    }

}

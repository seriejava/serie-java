/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package series.java;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.jsoup.Jsoup;

/**
 *
 * @author msdos
 */
public class noxvideo {
 private static final String patron_src = "src\\s*:\\s*[\\'\"]([^\\'\"]+)[\\'\"]";   
 private static final String nox_video_url = "http://www.nowvideo.to/embed/?v=a53f073ffbfc3";
 private static final String nox_donwload_file = "http://www.nowvideo.sx/download.php?file=";
 
   private noxvideo(){
   
   }
   public static String obtener_noxvideo(String url) throws Exception{
       
    String doc = http(url);
    
      doc = obtener_links(doc);
      doc = http(doc);
      doc = doc.substring(doc.indexOf("media=")+"media=\"".length(),doc.indexOf("_dash"));
      doc = String.format(nox_donwload_file.concat("%s.mp4"), doc);
     return doc;  
   }
   private static String   obtener_links(String doc) {
   Matcher m = Pattern.compile(patron_src).matcher(doc);
   return  m.find() ?  m.group(1) : null; 
   }
   private static String http(String url) throws Exception{
    return  Jsoup.connect(url).userAgent("Mozilla").timeout(3000*1000).followRedirects(true).ignoreContentType(true).get().toString();
   }
}

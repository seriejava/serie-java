package series.java;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.json.JSONArray;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class Main {

    private static final String[][] p = {{"43","38","25","16","5","12","28","39","30","45","27","46","18","3","23","1","34","15","4","20","7","2","6","47","40","35","11","33","17","19","41","26","44","21","42"},{"http://allvid.ch","https://clicknupload.link","http://cloudshares.net","http://www.datafile.com","https://www.flashx.tv","http://gamovideo.com","http://hugefiles.net","http://letwatch.us","http://mediafree.co","http://waaw.tv","http://nitroflare.com","http://www.nowdownload.to","http://www.nowvideo.to","https://openload.co","http://playedto.me","http://powvideo.net","http://rockfile.eu","http://salefiles.com","http://streamcloud.eu","http://streame.net","http://streamin.to","http://streamplay.to","http://thevideo.me","http://uploadrocket.net","https://uploads.to","http://uptobox.com","http://vidabc.com","http://vidgot.com","http://vidtodo.com","http://videowood.tv","http://vidzi.tv","http://watchvideo.us","https://www.wholecloud.net","http://youwatch.org","http://zstream.to"}};

    public static void main(String[] args) throws Exception {

        //Mostrar listado de series > mostarlistaseries("http://hdfull.tv/series/list");
        // Cargar temporadas > 
        //Mostrartemporadas("http://hdfull.tv/serie/hawaii-five-0");
        //** mostrar lista de servidores***************
        //url http://hdfull.tv/serie/hawaii-five-0/temporada-7/episodio-23;
       MostrarListaservidores("http://hdfull.tv/serie/seven-mortal-sins/temporada-1/episodio-4");
        // Mostrar captilos
        // Mostrarcapitulos("http://hdfull.tv/serie/the-flash-2014/temporada-1");
       // Mostrarcapitulos("http://hdfull.tv/serie/hawaii-five-0/temporada-7");
    }

    private static String mostarlistaseries(String url) throws Exception {
        Document doc = http(url);
        Elements tabla_list = doc.select(".show-complete-list a:not(div)");
        for (Element listado : tabla_list) {
            String name = listado.attr("title");
            String url_serie = listado.absUrl("href");
            System.out.println("Nombre: " + name + " Enlace: " + url_serie);

        }
        return null;

    }

    private static String Mostrartemporadas(String url) throws Exception {
        Document doc = http(url);
        System.err.println("Caratula : >" + Mostrarcaratula(doc));
        System.out.println("Descripcion: >" + obtenerdescripcion(doc));
        Elements temporadas = doc.select("#season-list > li > a");
        for (int i = 1; i < temporadas.size(); i++) {
            String link_temporada = temporadas.get(i).absUrl("href");
            System.out.println("numero temporada " + i + " enlace  " + link_temporada);

        }

        return null;
    }

    private static String Mostrarcaratula(Document doc) throws Exception {
        return doc.select(".show-poster > img").get(0).attr("abs:src");

    }

    private static String Mostrarcapitulos(String url) throws Exception {
        Document doc = http(url);
        int numtemporada = Integer.parseInt(url.substring(url.indexOf("temporada")).split("-")[1]);
        int numeroserie = encontrar_id_serie(doc.html().toString());
        JSONArray obj = post_capitulos(numtemporada, numeroserie);
        if (obj.length() > 0) {
            for (int i = 0; i < obj.length(); i++) {
                JSONObject objeto = obj.getJSONObject(i);
                String season = objeto.get("season").toString();
                String episode = objeto.get("episode").toString();
                String img_cap = objeto.get("thumbnail").toString();
                JSONObject title = (JSONObject) objeto.get("title");
                String title_es = (!title.isNull("es") || title.isNull("en")) ? title.getString("es") : title.getString("en");
                System.out.println("img: " + "http://hdfull.tv/tthumb/220x124/" + img_cap + " Capitulos " + season + "x" + episode + "  " + title_es + " enlace " + String.format(url.concat("\\/episodio-%s"), episode));
            }
        }
        return "capitulos no encontrado";
    }

    private static void MostrarListaservidores(String url) throws Exception {
       
        Document doc = http(url);
        String data = new hdfull(doc.html().toString()).dataHtml;
          List<Servidores> servidores = new ArrayList<>();
        Servidores server = null;
        Lista_servidores listas = null;
        System.out.println("Descripcion: >"+obtenerdescripcion(doc));
        JSONArray array = new JSONArray(data);
        for (int i = 0; i < array.length(); i++) {
            JSONObject obj = (JSONObject) array.get(i);
            int proid = obj.getInt("provider");
            String code = obj.getString("code");
            String idioma = obj.getString("lang");
            for (int j = 0; j < p[0].length; j++) {
                int a = Integer.parseInt(p[0][j]);
                if (a == proid) {
                     boolean encontrado = false;
                     String nombre_server =  p[1][j]  + " id " +j;
                       String nombreserver = p[1][j];
                 nombreserver = p[1][j].substring(nombreserver.indexOf("//")+"//".length(),nombreserver.lastIndexOf("."));
              
                //primero los  instancio;
              //  
                listas = new Lista_servidores();
                   
                    
                  
             
                  
              
                
                 
                    
                    System.out.println(nombreserver + j);
                    
                    Iterator<Servidores> it = servidores.iterator();
                    //buscando si existe   si existe ponemos a true
                    while (it.hasNext() && !encontrado) {
                        server = it.next();
                        if(server.getNombre().equals(nombreserver)){
                            encontrado = true;
                        
                        }
                        }
                   if(!encontrado){
                   //    System.err.println("Servidores no " +nombreserver);
                   //instanciamos la clases  para agrupar el servidor
                   server = new Servidores();
                   servidores.add(server);
                    
                    //server.getListaservidores().add(listas);
                   }
                   server.getListaservidores().add(listas);
                   server.setNombre(nombreserver);
                   listas.setEnlace(nombre_server.concat("/")+code);
                  // servidores.add(server);
                  
                }
            }

        }
        for (int i = 0; i < servidores.size(); i++) {
          System.err.println("******************** "+servidores.get(i).getNombre());  
            for (Lista_servidores s : servidores.get(i).getListaservidores()) {
                System.err.println(s.getEnlace());
            }
        }
        

    }

    private static String obtenerdescripcion(Document doc) {
        return doc.select(".show-overview-text").get(0).text();
    }

    private static JSONArray post_capitulos(int numerotemporada, int numeroserie) throws Exception {
        Document doc = Jsoup.connect("http://hdfull.tv/a/episodes").userAgent("Mozilla/5.0 (Windows NT 10.0; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0").referrer("http://hdfull.tv").data("action", "season", "start", "0", ",limit", "0", "show", String.valueOf(numeroserie), "season", String.valueOf(numerotemporada)).ignoreContentType(true).post();
        //  System.out.println(doc.text().toString());     
        return new JSONArray(doc.text());
    }

    private static Document http(String url) throws Exception {
        return Jsoup.connect(url).userAgent("Mozilla/5.0 (Windows NT 10.0; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0").referrer("http://hdfull.tv").get();
    }

    private static int encontrar_id_serie(String data) {
        String sid = "var sid = '(\\d+)'";
        Matcher m = Pattern.compile(sid).matcher(data);
        while (m.find()) {
            return Integer.parseInt(m.group(1));
        }
        return -1;

    }
}

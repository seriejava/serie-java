package series.java;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

public class Downloader {
	private static final String User_Agent = "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0";
	public static String http(String url,String referer) throws Exception {
	URLConnection con = new URL(url).openConnection();
	con.setRequestProperty("User-Agent", User_Agent);
	con.setRequestProperty("Referer", referer);
	BufferedReader  br = new BufferedReader(new InputStreamReader(con.getInputStream()));
	String linea = "";
	StringBuilder b = new StringBuilder();
	while((linea=br.readLine())!=null){
	b.append(linea).append("\n");	
	}
    br.close();
	return b.toString();
	}

}

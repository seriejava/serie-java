/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package series.java;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author msdos
 */
public class Servidores {
 private String  nombre;
 private List<Lista_servidores> listaservidores = new ArrayList<>();

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public List<Lista_servidores> getListaservidores() {
        return listaservidores;
    }

    public void setListaservidores(List<Lista_servidores> listaservidores) {
        this.listaservidores = listaservidores;
    }

    @Override
    public String toString() {
        return "Servidores{" + "nombre=" + nombre + ", listaservidores=" + listaservidores + '}';
    }
 
}

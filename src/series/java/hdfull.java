package series.java;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.bind.DatatypeConverter;

public class hdfull {

    public String dataHtml;

    public hdfull(String data) {
        String patron = "";
        Matcher matches;
        patron = "var ad\\s*=\\s*'([^']+)'";
        matches = Pattern.compile(patron).matcher(data);

        String data_obf = "";
        while (matches.find()) {
            data_obf = matches.group(1);
        }
        String data_decrypt = obfs(new String(DatatypeConverter.parseBase64Binary(data_obf)));

        dataHtml = data_decrypt;
    }

    private static String obfs(String data) {
        int n = 126 - 15;
        String[] chars = data.split("");
        String ret = "";
        for (Integer i = 0; i < chars.length; i++) {
            try {
                int c = Character.codePointAt(chars[i], 0);
                if (c < 126) {
                    chars[i] = new String(Character.toChars((c + n) % 126));
                    ret += chars[i];
                }
            } catch (Exception e) {
            }
        }
        return ret;
    }

}
